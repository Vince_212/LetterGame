package Sac;
import static org.junit.Assert.*;

import org.junit.Test;

public class TestPotCommun {
	@Test
	public final void testAjouterLettre() {
		PotCommun potCommun = new PotCommun();
		int initSize = potCommun.toString().length();
		Character a = 'a';
		potCommun.ajouterLettre(a);
		assertEquals(initSize, potCommun.toString().length());
	}
	
	@Test
	public final void testEnleverLettre() {
		PotCommun potCommun = new PotCommun();
		Character a = 'a';		
		int initSize = potCommun.toString().length();

		if (potCommun.toString().length()== 0) {
			potCommun.enleverLettre(a);
			fail("Plus de lettre dans le pot commun");
		}
		
		potCommun.ajouterLettre(a);
		potCommun.ajouterLettre(a);
		assertEquals(initSize+2, potCommun.toString().length());
		potCommun.ajouterLettre('_');
		assertEquals(potCommun.toString().length(), initSize+2);
		}
	
	@Test
	public final void testEnleverMot() {
		PotCommun potCommun = new PotCommun();
		potCommun.ajouterLettre('b');
		potCommun.ajouterLettre('o');
		potCommun.ajouterLettre('n');
		potCommun.ajouterLettre('j');
		potCommun.ajouterLettre('o');
		potCommun.ajouterLettre('u');
		potCommun.ajouterLettre('r');

		int initSize = potCommun.toString().length();
		potCommun.enleverMot("jour");
		assertEquals(initSize-4, potCommun.toString().length());
		assertEquals("bon", potCommun.toString());
	}
	
	@Test
	public final void testTirerUneLettre() {
		PotCommun potCommun = new PotCommun();

		String alpha = "abcdefghijklmnopqrstuvwxyz";
		char randChar = potCommun.tirerUneLettre();
		for (int i = 0; i < alpha.length(); i++) {
			if(alpha.charAt(i) == randChar) {
				assertTrue(true);
			}
		}
		assertFalse(true);		
	}
}
