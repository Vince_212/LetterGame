package Jeu;
import java.util.ArrayList;

public class Joueur {
	
	private static int counter = 1;
	public final int idJoueur;
	public ArrayList<String> mots;
	
	public Joueur() {
		mots = new ArrayList<String>();
		this.idJoueur = counter++;
	}
	
	public void ajouterMot(String mot){
		mots.add(mot);
	}
	
	public void supprimerMot(String mot){
		mots.remove(mot);
	}
	
	@Override
	public String toString() {
		return "Joueur : "+ idJoueur;
	}

	public int afficherScore() {
		return mots.size();
	}
	
	public void afficherMots() {
		for (int i = 0; i < mots.size(); i++) {
			System.out.println(mots.get(i));
		}
	}
}
