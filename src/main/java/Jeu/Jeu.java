package Jeu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Sac.PotCommun;
import fr.esiea.unique.binome.name.dictionary.Dictionary;

public class Jeu
{
	public Dictionary dico;
	public PotCommun pot;
	BufferedReader br;
	
	Joueur joueur1 = new Joueur();
	Joueur joueur2 = new Joueur();
	
	public Jeu(){
		dico = new Dictionary();
		pot = new PotCommun();
		br = new BufferedReader(new InputStreamReader(System.in));
		}
	
//	public Jeu(PotCommun potCommun, Joueur... joueurs) { //Enable implements of multipel players
//		this.potCommun = potCommun;
//		this.joueur1 = new Joueur();
//		this.joueur2 = new Joueur();			
//	}
	

	public void preparationJeu() throws IOException
	{
		char j1 = pot.tirerUneLettre();
		pot.ajouterLettre(j1);
		char j2 = pot.tirerUneLettre();
		pot.ajouterLettre(j2);
		
		System.out.println("Le joueur 1 a tiré : " + j1 + ".");
		System.out.println("Le joueur 2 a tiré : " + j2 + ".");
		System.out.println(pot);
		
		int i = quiCommence(j1, j2);

		if (i == 1) { //Joueur 1
			tourDeJeu(joueur1);
		}
		else { //Joueur 2
			tourDeJeu(joueur2);
		}
	}
	
	public int quiCommence(char j1, char j2)
	{
		if (j1 == j2) {
			System.out.println("Le joueur 1 & 2 ont la meme lettre. Joueur 1 commence.\n");
		} 
		else if (j1 < j2) {
			System.out.println("Le joueur 1 commence.\n");
		}
		else {
			System.out.println("Le joueur 2 commence.\n");
			return 2;
		}
		return 1;	
	}

	public void tourDeJeu(Joueur joueurSelectionne) throws IOException {
		//Debut tour
		pot.ajouterLettre(pot.tirerUneLettre());
		pot.ajouterLettre(pot.tirerUneLettre());
		
		System.out.println("Le score du joueur 1 est de " + joueur1.afficherScore() + " et son score de : ");
		joueur1.afficherMots();
		
		System.out.println("Le score du joueur 1 est de " + joueur2.afficherScore() + " et son score de : ");
		joueur2.afficherMots();
		
		System.out.println("Tour du joueur " + joueurSelectionne.idJoueur);
		
		System.out.println("Les lettres suivantes sont dans le pot commun : ");
		pot.toString();
		
		choisirActionDuJoueur(joueurSelectionne);
		
		//Fin du tour
		if(joueurSelectionne.mots.size() >= 10) {
			System.out.println("Le " + joueurSelectionne + " a gagne ! Fin de la partie.");
			return;
		} else if(joueurSelectionne.idJoueur == 2) {
			tourDeJeu(joueur1);
		}
		else {
			tourDeJeu(joueur2);
		}
	}

	private void choisirActionDuJoueur(Joueur joueurSelectionne) throws IOException {
		System.out.println("Tapez 1 pour : Proposer un mot.");
		System.out.println("Tapez 2 pour : Voler un mot a l'adversaire.");
		System.out.println("Tapez 3 pour : Passer votre tour.");
		
		String action = "0";
		do {
			switch (action) {
			case "1": //Rentrer mot
				rentrerMot(joueurSelectionne);
				break;
			
			case "2": //Voler mot
				volerMot(joueurSelectionne);
				break;
			
			case "3": //Passer son tour
				return;				
			
			default:
				System.out.println("Merci d'entrer un caractere valide");
				break;
			}
			action = br.readLine();
		}  while (action != "1" || action != "2" || action != "3");
	}

	public void rentrerMot(Joueur joueurSelectionne) throws IOException {
		String mot = "";
		System.out.println("Entrez voter mot : ");
		
		do {
			mot = br.readLine();
		} while (mot == "");
		
		Dictionary dico = new Dictionary();
		dico.isWord(mot);
		
		joueurSelectionne.ajouterMot(mot);
	}
	
	public void volerMot(Joueur joueurSelectionne) throws IOException {
		String mot = "";
		System.out.println("Ecrire le mot a voler : ");
		String motVolee = "";
		do {
			motVolee = br.readLine();
		} while (motVolee == "");
		
		System.out.println("Ecrire le nouveau mot : ");
		
		if (joueurSelectionne.idJoueur == 1) {
			joueur2.supprimerMot(motVolee);
		} else {
			joueur1.supprimerMot(motVolee);
		}
		joueurSelectionne.ajouterMot(mot);
	}
}
