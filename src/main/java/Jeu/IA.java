package Jeu;

import java.util.ArrayList;

import fr.esiea.unique.binome.name.dictionary.Dictionary;

public class IA  extends Joueur {
	public Dictionary dico;
	private ArrayList<String> combinaisons;
	
	public ArrayList<String> mots;


	public IA() {
		mots = new ArrayList<String>();
		dico = new Dictionary();
	}
	
	public void priseDecision(String pot, Joueur joueurAdverse) {
		if (pot.length() >= 3 & joueurAdverse.mots.size() >= 3) { //Contrainte permettant de laisser une chance au joueur.
			String motVole = volerMot(pot, joueurAdverse);
			
			if (motVole != "") {
				joueurAdverse.supprimerMot(motVole);
				mots.add(motVole);
			}
			else {
				String motPossible = ecrireMot(pot);
				
				if (motPossible != "") {
					mots.add(motPossible);
				}
			}
		} else { passerTour(); }	
	}
	
	public String ecrireMot(String pot) {
		return "";
	}
	
	public String volerMot(String pot, Joueur joueurAdverse) {
		combinaisons = null;
		for (int i = 0; i < joueurAdverse.mots.size(); i++) {
			String motTest = joueurAdverse.mots.get(i);
			for (int j = 0; j < pot.length(); j++) {
				combinaisons.addAll(permutation (pot.charAt(i) + motTest));
			}
		}
		
		for (int i = 0; i < combinaisons.size(); i++) {
			if(dico.isWord(combinaisons.get(i))){
				return combinaisons.get(i);
			}
		}
		return "";
	}
	
	public void passerTour() {
		System.out.println("Je ne sais pas quoi faire, � ton tour.");
	}
	
	
	public static ArrayList<String> permutation(String s) {
	    // The result
	    ArrayList<String> res = new ArrayList<String>();
	    // If input string's length is 1, return {s}
	    if (s.length() == 1) {
	        res.add(s);
	    } else if (s.length() > 1) {
	        int lastIndex = s.length() - 1;
	        // Find out the last character
	        String last = s.substring(lastIndex);
	        // Rest of the string
	        String rest = s.substring(0, lastIndex);
	        // Perform permutation on the rest string and
	        // merge with the last character
	        res = merge(permutation(rest), last);
	    }
	    return res;
	}
	
	public static ArrayList<String> merge(ArrayList<String> list, String c) {
	    ArrayList<String> res = new ArrayList<String>();
	    // Loop through all the string in the list
	    for (String s : list) {
	        // For each string, insert the last character to all possible postions
	        // and add them to the new list
	        for (int i = 0; i <= s.length(); ++i) {
	            String ps = new StringBuffer(s).insert(i, c).toString();
	            res.add(ps);
	        }
	    }
	    return res;
	}


}
