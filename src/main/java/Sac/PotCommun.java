package Sac;

import java.util.ArrayList;
import java.util.Random;

public class PotCommun {
	private ArrayList<Character> potCommun;
	
	public PotCommun(){
		potCommun = new ArrayList<Character>();
	}
	
	public char tirerUneLettre(){
		Random randParam = new Random();
		char randChar = (char)(randParam.nextInt(26) + 'a');
		return randChar;
	}
	
	public void ajouterLettre(Character lettre){
		potCommun.add(lettre);
	}
	
	public void enleverLettre(Character lettre){
		potCommun.remove(lettre);
	}
	
	public void enleverMot(String mot){
		for(int i=0; i < mot.length(); i++){
			Character lettre = mot.charAt(i);
			enleverLettre(lettre);
		}
	}

	@Override
	public String toString() {
		return potCommun.toString();
	}
}
