package fr.esiea.unique.binome.name.dictionary;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;

public class Dictionary implements IDictionary {	

	private HashSet<String> mots;
	
	public Dictionary(){
		mots = Fichier();
	}
	
	
	private HashSet<String> Fichier() {
		String fichier = "src/main/resources/dico.txt";
		HashSet<String> hs = new HashSet<String>();
		BufferedReader br = null;
		try {//Ouvrir fichier
			InputStream ips = new FileInputStream(fichier);
			InputStreamReader ipsr = new InputStreamReader(ips);
			br = new BufferedReader(ipsr);			
		} 
		catch (Exception e) {
			System.err.println(e.toString());
		}
		
		if(br != null){ //Lecture fichier = Remplissage arbre a  partir des donnes du fichier
			String currentLine;
			try{
				do{
					currentLine = br.readLine();
		            hs.add(currentLine);
				} while (br.ready());
				br.close();
			}catch (Exception e){
				System.err.println(e.toString());
			}
		}
		return hs;
	}

	@Override
	public boolean isWord(String motTest) {
		return mots.contains(motTest);
	}
}
