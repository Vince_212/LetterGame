# TP Architecture Logicielle / Inf4043 - 2017 - Jeux de lettres

- Date de rendu : 24/02/2017 23h - pas de retard accepté
- Binôme Pierre Roger & Constantin Jaïs
- Contacts : 
  - const.jais[at]gmail[dot]com
  - roger[at]esiea[dot]fr

## Règles du jeux 

- Objectif du jeux :
  - Le premier joueur ayant 10 mots gagne la partie

- Déroulement du jeux :
  - Chacun des joueurs tire une lettre aléatoire d'un sac, et les mettent au milieu dans le pot commun
  - Le joueur qui a tiré la lettre la plus petite lettre dans l'alphabet commence
  - Chaque fois que c'est le début du tour d'un joueur il tire deux lettres aléatoires qu'il rajoute au pot commun
  - Chaque fois qu'un joueur fait un mot il tire une lettre aléatoire qu'il rajoute au pot commun
  - Quand le joueur ne trouve plus de mot il passe et le joueur suivant commence son tour (par tirer 2 lettres qu'il rajoute au pot commun)

- Comment faire un mot ?
  - En utilisant uniquement les lettres du pot commun
  - En prenant un mot de ces adversaires (toutes les lettres du mot) et en lui rajoutant des lettres du pot commun
  - En rallongeant un de ces mots avec des lettres du pot commun ou en utilisant un autre mot (toutes les lettres)
  - Attention, seul les noms communs sont autorisés

- Pour faciliter :
  - les lettres possibles sont uniquement les 26 de l'alphabet (ex : é <-> e)
  - les mots composés sont considérés comme deux mots

- Pour les plus avancés :
  - Le cas des anagrammes :
    - On peut voler un mot en faisant un anagramme uniquement si il n'a pas déjà été fait. Bien entendu, faire un anagramme permet de tirer une nouvelle lettre.

## Objectif du TP

- Une première étape consiste à pouvoir jouer à plusieurs autour d'un même écran.
- Une interface en ligne de commande est suffisante.
- Nous attendons aussi a minima une de ces deux extensions (ou les deux pour les plus courageux :-)) :
  - Une architecture client / serveur, chaque joueur utilisant une instance d'un client pour jouer.
  - Une intelligence artificiel permettant de jouer contre l'ordinateur.
- Nous attendons aussi une description de votre architecture (Quel responsabilité à chaque package, ..).
- De plus, vous devrez illustrer trois principes SOLID ou design pattern en utilisant vos propres classes. 
  - pourquoi avez-vous utilisé ce design pattern / principe ? Qu'est-ce que cela vous a apporté ? Comment l'avez-vous appliqué ?
  - Nous attendons quelques paragraphes seulement
- Ces deux exercices sont à livrer dans le README.md du projet.


### Architecture de l'application

## Dictionnary
La classe Dictionnary est un singleton. 
Dictionnary utilise le dictionnaire fournis pour la réalisation du programme et dispose des fonctions suivantes :
- Charger le dictionnaire dans un arbre. Chaque branche de l'arbre est une solution possible.
- Tester la présence d'un mot dans son arbre.

## Pot commun
La classe PotCommun est un singleton qui gère les lettres commune entre les joueurs.
Ses fonctions sont les suivantes :
- Initialiasation
- Ajouter une lettre au pot,
- Retirer une lettre au pot,
- Supprimer un mot du pot,
- Afficher le pot.

## Joueur
La classe Joueur gère les mots créé par le joueur, son score et son ID.

## Jeu
La classe Jeu fait le lien entre potCommun, Joueur et Dictionnaire en s'assurant de faire respecté les règles du jeu.
Jeu initialise une partie, elle gère les tours de jeu ainsi que les messages qui doivent s'afficher dans la console.
- Initialiser la partie,
- Déterminer au hasard le joueur qui commence,
- Gerer les actions de jeu
- - Voler un mot,
- - Creer un mot,
- - Passer son tour.






| Points | Description           | 
| :----- |:-------------| 
|5 points | Architecture du code, découpage des classes, respect des principes Objects (SOLIDE), méthodes < 15 lignes... |
|5 points | La totalité des feature faites. Pas de bug et cas aux limites gérés  |
|3 points | Test : code coverage > 70%, assertions intelligentes dans les tests , tests unitaires |
|2 point  | Exercice Architecture & Design Pattern / Solid |
|2 points | Analyse statique de code findbug / PMD |
|2 point  | Utilisation de Maven (ou autre logiciel du même type) pour gérer les dépendances et construire le projet. Utilisation de git avec plusieurs commits pour chaque personnes du binome |
|1 point  | Conventions java / Maven respectées (Camelcase, package, ...) |